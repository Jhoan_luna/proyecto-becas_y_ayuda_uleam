//VALIDACION DE DATOS formulario 4//

const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
    cedula: /^\d{1,10}$/, // 1 a 10 numeros.
    edad: /^\d{1,2}$/, // 1 a 2 numeros.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    telefono: /^\d{7,14}$/ // 7 a 14 numeros.
}

const validarFormulario = (e) =>{

    switch (e.target.name){

        case "cedula":
            if(expresiones.cedula.test(e.target.value)){

                document.getElementById('grupo_cedula').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_cedula').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_cedula .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_cedula').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_cedula').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_cedula .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;
        case "nombre":
            if(expresiones.nombre.test(e.target.value)){

                document.getElementById('grupo_nombre').classList.remove('formulario__grupo-incorrecto');
                document.getElementById('grupo_nombre').classList.add('formulario__grupo-correcto');
                document.querySelector('#grupo_nombre .formulario__input-error').classList.remove('formulario__input-error-activo');

            } else{

                document.getElementById('grupo_nombre').classList.add('formulario__grupo-incorrecto');
                document.getElementById('grupo_nombre').classList.remove('formulario__grupo-correcto');
                document.querySelector('#grupo_nombre .formulario__input-error').classList.add('formulario__input-error-activo');

            }
        break;

    case "apellido":
        if(expresiones.nombre.test(e.target.value)){

            document.getElementById('grupo_apellido').classList.remove('formulario__grupo-incorrecto');
            document.getElementById('grupo_apellido').classList.add('formulario__grupo-correcto');
            document.querySelector('#grupo_apellido .formulario__input-error').classList.remove('formulario__input-error-activo');

        } else{

            document.getElementById('grupo_apellido').classList.add('formulario__grupo-incorrecto');
            document.getElementById('grupo_apellido').classList.remove('formulario__grupo-correcto');
            document.querySelector('#grupo_apellido .formulario__input-error').classList.add('formulario__input-error-activo');

        }
    break;

    case "direccion":
        if(expresiones.nombre.test(e.target.value)){

            document.getElementById('grupo_direccion').classList.remove('formulario__grupo-incorrecto');
            document.getElementById('grupo_direccion').classList.add('formulario__grupo-correcto');
            document.querySelector('#grupo_direccion .formulario__input-error').classList.remove('formulario__input-error-activo');

        } else{

            document.getElementById('grupo_direccion').classList.add('formulario__grupo-incorrecto');
            document.getElementById('grupo_direccion').classList.remove('formulario__grupo-correcto');
            document.querySelector('#grupo_direccion .formulario__input-error').classList.add('formulario__input-error-activo');

        }
    break;

    case "telefono":
        if(expresiones.telefono.test(e.target.value)){

            document.getElementById('grupo_telefono').classList.remove('formulario__grupo-incorrecto');
            document.getElementById('grupo_telefono').classList.add('formulario__grupo-correcto');
            document.querySelector('#grupo_telefono .formulario__input-error').classList.remove('formulario__input-error-activo');

        } else{

            document.getElementById('grupo_telefono').classList.add('formulario__grupo-incorrecto');
            document.getElementById('grupo_telefono').classList.remove('formulario__grupo-correcto');
            document.querySelector('#grupo_telefono .formulario__input-error').classList.add('formulario__input-error-activo');

        }
    break;

    case "edad":
        if(expresiones.edad.test(e.target.value)){

            document.getElementById('grupo_edad').classList.remove('formulario__grupo-incorrecto');
            document.getElementById('grupo_edad').classList.add('formulario__grupo-correcto');
            document.querySelector('#grupo_edad .formulario__input-error').classList.remove('formulario__input-error-activo');

        } else{

            document.getElementById('grupo_edad').classList.add('formulario__grupo-incorrecto');
            document.getElementById('grupo_edad').classList.remove('formulario__grupo-correcto');
            document.querySelector('#grupo_edad .formulario__input-error').classList.add('formulario__input-error-activo');

        }
    break;
    
    }
}

inputs.forEach((input) => {

    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);

})

formulario.addEventListener('submit', (e) => {
    e.preventDefault();
})
